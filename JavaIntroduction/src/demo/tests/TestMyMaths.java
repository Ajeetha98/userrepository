package demo.tests;

import static org.junit.Assert.*;
import demo.intro.*;

import org.junit.Test;

public class TestMyMaths {
	MyMaths maths = new MyMaths();
	@Test
	public void myFactorialOneOrZero() {
		double expected = 1;
		double actual = maths.myFactorial(1);
		assertEquals(expected, actual, 0.0000000001);
		actual = maths.myFactorial(0);
		assertEquals(expected, actual, 0.0000000001);
	}
	
	@Test
	public void myFactorialOverOne() {
		double expected = 6;
		double actual = maths.myFactorial(3);
		assertEquals(expected, actual, 0.0000000001);
	}

	@Test
	public void fizzBuzzForThree() {
		String expected = "Fizz";
		String actual = maths.fizzBuzz(9);
		assertEquals(expected, actual, 0.0000000001);
	}
//	@Test
//	public void fizzBuzzForFive() {
//		String expected = "Buzz";
//		String actual = maths.fizzBuzz(10);
//		assertEquals(expected, actual, 0.0000000001);
//		
//	}
//	@Test
//	public void fizzBuzzForBoth() {
//		String expected = "FizzBuzz";
//		String actual = maths.fizzBuzz(15);
//		assertEquals(expected, actual, 0.0000000001);
//		
//	}
//	@Test
//	public void fizzBuzzForNon() {
//		String expected = "23";
//		String actual = maths.fizzBuzz(23);
//		assertEquals(expected, actual, 0.0000000001);
//		
//	}
}
