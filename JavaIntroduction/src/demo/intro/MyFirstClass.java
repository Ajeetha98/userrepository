package demo.intro;

public class MyFirstClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("My First Java Exercise");
		String make, model;
		double engineSize;
		byte gear;
		make = "BMW";
		model = "530D";
		engineSize = 3.0;
		gear = 2;
		System.out.println("make : "+ make +"\nmodel : "+model +"\nEngineSize : "+ engineSize + "\ngear : "+gear);
		short speed;
		speed = (byte)(gear * 20);
		System.out.println("\nSpeed :" + speed);
		int revs = (int)(speed * gear);
		System.out.println("\nrevs :" + revs);
	}

}
