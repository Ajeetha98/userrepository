package demo.intro;

public class MyMaths {
	public double myFactorial(int n) {
		if (n == 1 || n == 0 )
			return 1;
		else {
			return myFactorial(n - 1) * n;
		}
	}

	public String fizzBuzz(int n) {
		if(n % 3 == 0 && n % 5 == 0)
			return "FizzBuzz";
		else if(n % 3 == 0 )
			return "Fizz";
		else if(n % 5 == 0)
			return "Buzz";
		else
			return String.valueOf(n);
}
}
