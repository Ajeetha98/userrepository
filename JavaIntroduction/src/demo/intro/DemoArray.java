package demo.intro;

public class DemoArray {
	
	public static void createArray() {
		int[] numbers;
		numbers = new int[5];
		
		Student[] students = new Student[4];
		
		for(int i=0; i<numbers.length; i++) {
			numbers[i] = i+1;
			System.out.println(numbers[i]);
		}	
		
		for(int i=0; i<students.length; i++) {
			students[i] = new Student();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		createArray();
	}

}
