package demo.intro;

public class UseProduct {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Product aProduct = new Product();
		aProduct.setName("Giant Road Bike");
		aProduct.setPrice(500.00);
		System.out.printf("%s costs %.2f\n",aProduct.getName(),aProduct.getPrice());
		
		Product bProduct = aProduct;
		bProduct.setPrice(600);
		System.out.printf("%s costs %.2f",aProduct.getName(),aProduct.getPrice());
	}

}
