package demo.intro;
import java.util.Date;
public class Employee {
	private String name;
	private int salary;
	private int id;
	private Date dateOfJoining;
	public static int minSalary = 7000;
	public static int empId = 0;
	
	public static int getMinSalary() {
		return minSalary;
	}
	public static void setMinSalary(int minSalary) {
		Employee.minSalary = minSalary;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public Date getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	
	public void payRaise(int amount) {
		this.salary += amount;
	}
	
	public String toString() {
		return String.format("Name - " + this.name + " Id - " + this.id + " Salary - " + this.salary + " Date Of Joining - "+ this.dateOfJoining);
	}
	public Employee(String name, int salary) {
		super();
		this.name = name;
		if(salary < minSalary) {
			salary = minSalary;
		}
		this.salary = salary;
		this.dateOfJoining = new Date();
		this.id = ++empId;
		
	}
	public Employee(String name) {
	
		this(name,minSalary);
	}
	
	public void payBonus() {
		this.salary += 0.1*salary;
	}
	public void payBonus(double bonusPercent) {
		this.salary += (bonusPercent/100)*salary;
	}
	public void payBonus(double bp,int minSalary,int maxSalary) {
		if (salary >= minSalary && salary <= maxSalary) {
			payBonus(bp);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
