package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Users;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserRepository repository;
	 @RequestMapping(value = "/home", method = RequestMethod.GET)
		
		@ResponseBody
		public ModelAndView home() { 
			ModelAndView modelAndView = new ModelAndView(); 
			modelAndView.setViewName("home"); 
			return modelAndView; 
			}

		@RequestMapping(value = "/register", method = RequestMethod.GET)
		
		@ResponseBody
		public ModelAndView register() { 
			ModelAndView modelAndView = new ModelAndView(); 
			modelAndView.setViewName("register"); 
			return modelAndView; 
			}
		
@RequestMapping(value = "/idForm", method = RequestMethod.GET)
	
	@ResponseBody
	public ModelAndView idForm() { 
		ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("idForm"); 
		return modelAndView; 
		}
@RequestMapping(value = "/data", method = RequestMethod.GET)

@ResponseBody
public ModelAndView data() { 
	ModelAndView modelAndView = new ModelAndView(); 
	modelAndView.setViewName("valid"); 
	return modelAndView; 
	}

@RequestMapping(value = "/update", method = RequestMethod.GET)

@ResponseBody
public ModelAndView update() { 
	ModelAndView modelAndView = new ModelAndView(); 
	modelAndView.setViewName("update"); 
	return modelAndView; 
	}
		
		
	  
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Users> getAllPets() {
	  return repository.findAll();
	}
     @RequestMapping(value = "/register", method = RequestMethod.POST)
	public Users createPet(@Valid @RequestBody Users user  ) {
//	    Users userCreated =new Users();	
    	 user.set_id(ObjectId.get());
//		userCreated.setEmailId(email);
//		userCreated.setUserName(userName);
//		userCreated.setPassword(password);
		repository.save(user);
		return user;
	}
 

	@RequestMapping(value = "/login/{emailId}/{password}", method = RequestMethod.GET)
	public String getPetById(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
	  List <Users> user = new ArrayList<Users>();
	  user = repository.findAll();
	  
	  for(Users u: user) {
		  
		  if(u.getEmailId().equals(emailId) && u.getPassword().equals(password)) {
			  return "Logged In Successfully";
		  }
	  }
	  return "User Not Registered";
	}
    @RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	
	@ResponseBody
	public Users show(@PathVariable("id") String id) { 
    	List <Users> user = new ArrayList<Users>();
  	  user = repository.findAll();
  	  
  	  for(Users u: user) {
  		  
  		  if(u.get_id().equals(id) ) {
  			  return u;
  		  }
  	  }
  	  return null;
  	}
		 


	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public void modifyPetById(@PathVariable("id") ObjectId id,@Valid @RequestBody Users userCreated) {
	 
	     userCreated.set_id(id);
	  repository.save(userCreated);
	  
	}
	
	
	

}
