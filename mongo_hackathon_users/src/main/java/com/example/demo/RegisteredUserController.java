package com.example.demo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/reg")
public class RegisteredUserController {
	@Autowired
	private UserRepository repository;
	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping(value = "/{id}/{symbol}", method = RequestMethod.GET)
	public ResponseEntity<String> getPetById(@PathVariable("id") ObjectId id, @PathVariable("symbol") String symbol) {
		if(repository.existsBy_id(id))
		{
			String urlBuilder =  "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords="+symbol+"&apikey=CRJCMSWKG5DHMKU8";
			//Boolean test;
			//test = urlBuilder.equals("https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=ibm&apikey=CRJCMSWKG5DHMKU8");
			//System.out.println(test);
			//String url = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + symbol + "&keywords=ibm&apikey=CRJCMSWKG5DHMKU8";
			return restTemplate.getForEntity(urlBuilder, String.class);
		}
		else
		{
			return new ResponseEntity<>("Not a registered user",HttpStatus.BAD_REQUEST);
			
			
			
		}
	}
}
