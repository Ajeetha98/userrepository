package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Person;

@RestController
public class MyRestController {
	@Autowired
	Person aPerson;
	
	@RequestMapping("/hello")
	public String demoHello(@RequestParam String name){
		return("Hello " + name);
	}
	
	@RequestMapping("/pet")
	public String demoUsePerson(){
		return(aPerson.feedPet());
	}
}

