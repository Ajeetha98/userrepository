package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Person {
	private Pet myPet;
	
	@Autowired
	public Person(Pet myPet) {
		this.myPet = myPet;
	}
	
	public String feedPet() {
		return myPet.feed();
	}
}
