package com.example.demo.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
	@Id
	public ObjectId _id;
	public String stockTicker;
	public String dateCreated;
	public int stockQuantity;
	public double requestedPrice;
	public String tradeStatus;
	
	public Trade(String stockTicker, String dateCreated, int stockQuantity, double requestedPrice, String tradeStatus) {
		super();
		this.stockTicker = stockTicker;
		this.dateCreated = dateCreated;
		this.stockQuantity = stockQuantity;
		this.requestedPrice = requestedPrice;
		this.tradeStatus = tradeStatus;
	}
	
	public String get_id() { return _id.toHexString(); }
	public void set_id(ObjectId _id) { this._id = _id; }

	public String getStockTicker() {
		return stockTicker;
	}

	
	public String getDateCreated() {
		return dateCreated;
	}

	

	public int getStockQuantity() {
		return stockQuantity;
	}

	

	public double getRequestedPrice() {
		return requestedPrice;
	}

	

	public String getTradeStatus() {
		return tradeStatus;
	}

	
	@Override
	public String toString() {
		return "Trade [stockTicker=" + stockTicker + ", dateCreated=" + dateCreated + ", stockQuantity=" + stockQuantity
				+ ", requestedPrice=" + requestedPrice + ", tradeStatus=" + tradeStatus + "]";
	}
	
	
	
}
