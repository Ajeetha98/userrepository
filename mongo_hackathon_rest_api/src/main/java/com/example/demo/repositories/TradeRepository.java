package com.example.demo.repositories;

import com.example.demo.model.Trade;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, String> {
	Trade findBy_id(ObjectId _id);
}
