package com.example.demo;

import com.example.demo.model.Trade;
import com.example.demo.repositories.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/trades")
public class TradeController {
	@Autowired
	private TradeRepository repository;
		  
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Trade> getAllTrades() {
	  return repository.findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Trade getPetById(@PathVariable("id") ObjectId id) {
	  return repository.findBy_id(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void modifyPetById(@PathVariable("id") ObjectId id, @Valid @RequestBody Trade trades) {
	  trades.set_id(id);
	  repository.save(trades);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Trade createPet(@Valid @RequestBody Trade trades) {
	  trades.set_id(ObjectId.get());
	  repository.save(trades);
	  return trades;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deletePet(@PathVariable ObjectId id) {
	  repository.delete(repository.findBy_id(id));
	}
}