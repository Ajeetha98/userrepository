package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoHackathonRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoHackathonRestApiApplication.class, args);
	}

}
