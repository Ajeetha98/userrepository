package demo.mongodb;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import java.util.Iterator;

public class MongoDbOps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//connectAndRetrieveDocuments();
		//retrieveOneDocument();
		//addDocument();
		deleteOneDocument();
		//updateDocument();
		retrieveSomeDocuments();
	}
	
	public static void connectAndRetrieveDocuments() {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("peopledb");
		System.out.println("Connected to MongoDB on localhost");
		MongoCollection<Document> mycollection = database.getCollection("people");
		FindIterable<Document> iterDoc = mycollection.find();
		Iterator<Document> it = iterDoc.iterator();
		while(it.hasNext()) {
			System.out.println(it.next().getString("name"));
			//System.out.println(it.next());
		}
		myMongo.close();
	}
	
	public static void retrieveOneDocument() {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("peopledb");
		MongoCollection<Document> mycollection = database.getCollection("people");
		Document myDoc = mycollection.find(Filters.eq("name","Ishita")).first();
		System.out.println(myDoc.getString("name") + ", " + myDoc.getDouble("age"));
		System.out.println(myDoc.toJson());
		myMongo.close();
	}
	
	private static void retrieveSomeDocuments() {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("peopledb");
		MongoCollection<Document> mycollection = database.getCollection("people");
		MongoCursor<Document> cursor = mycollection.find(Filters.and(Filters.gte("age",40.0),
				Filters.lte("age", 50.0))).iterator();
		while(cursor.hasNext()) {
			Document adoc = cursor.next();
			System.out.println(adoc.getString("name") + ", " + (double)adoc.getDouble("age")
			+ adoc.getString("gender"));
		}
		myMongo.close();
	}
	
	private static void addDocument() {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("peopledb");
		Document newPerson = new Document("name", "Colleen").append("age", 48.0);
		MongoCollection<Document> mycollection = database.getCollection("people");
		mycollection.insertOne(newPerson);
		myMongo.close();
	}
	
	private static void updateDocument() {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("peopledb");
		Document aPerson = new Document("name", "Colleen").append("age", 48.0).append("gender", "F");
		MongoCollection<Document> mycollection = database.getCollection("people");
		mycollection.updateMany(Filters.eq("name", "Colleen"), new Document("$set", aPerson));
		myMongo.close();
	}
	
	private static void deleteOneDocument() {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("peopledb");
		MongoCollection<Document> mycollection = database.getCollection("people");
		//mycollection.updateOne(Filters.eq("name", "Colleen"), new Document("$set", aPerson));
		DeleteResult deleteResult= mycollection.deleteMany(Filters.eq("name", "Colleen"));
		System.out.println("Document delete count is: " + deleteResult.getDeletedCount());
		myMongo.close();
	}
}