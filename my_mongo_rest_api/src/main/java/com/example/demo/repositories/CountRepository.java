package com.example.demo.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Count;


public interface CountRepository extends MongoRepository<Count, String> {
	Count findBy_id(ObjectId _id);
}
