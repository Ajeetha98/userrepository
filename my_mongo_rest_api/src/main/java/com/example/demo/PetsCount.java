package com.example.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Count;
import com.example.demo.model.Pets;
import com.example.demo.repositories.CountRepository;
import com.example.demo.repositories.PetsRepository;



@RestController
@RequestMapping("/petsCount")
public class PetsCount {
	@Autowired
	private PetsRepository repository;
	@Autowired
	private CountRepository repository1;
	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Count> getAllPetsCount() {
		  return repository1.findAll();
		}
	
	public List<Count> getAllPets1() {
	  List<Pets> myList = new ArrayList<Pets>();
	  List <Count> myList1 = new ArrayList<Count>();
	  myList = repository.findAll();
	  myList1 = repository1.findAll();
	  for (Pets p: myList) {
		  System.out.println("outer loop");
			  for(Count c:myList1) {
				  System.out.println(c.getSpeciesName() + p.getSpecies());
				  if(c.getSpeciesName().equals(p.getSpecies())) {
					  System.out.println("Checking");
					  modifyCountById(c.get_id(), new Count(c.getSpeciesName(),c.getSpeciesCount()+1));
					  c.setSpeciesCount(c.getSpeciesCount()+1);
					  System.out.println(c.getSpeciesCount());
				  }
				  else {
					  System.out.println("new record calling cons");
					  createPetCount(new Count(p.getSpecies(),1));
				  }
			  }
		  }
	  return repository1.findAll();
	  }
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Count createPetCount(@Valid @RequestBody Count count) {
	  System.out.println("creating new object");
	  count.set_id(ObjectId.get());
	  repository1.save(count);
	  return count;
	}
	public void modifyCountById(ObjectId id, Count count) {
		  count.set_id(id);
		  repository1.save(count);
		}
	

}
