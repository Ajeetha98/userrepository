package com.example.demo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Count {
	@Id
	  public ObjectId _id;
	  public String speciesName;
	  public int speciesCount;
	  
	  public Count() {}
	  
	public Count(ObjectId _id, String speciesName, int speciesCount) {
		super();
		this._id = _id;
		this.speciesName = speciesName;
		this.speciesCount = speciesCount;
	}
	
	public Count(String speciesName, int speciesCount) {
		super();
		this.speciesName = speciesName;
		this.speciesCount = speciesCount;
	}
	@Override
	public String toString() {
		return "Count [_id=" + _id + ", speciesName=" + speciesName + ", speciesCount=" + speciesCount + "]";
	}

//	public String get_id() {
//		return _id.toHexString();
//	}
//	public void set_id(ObjectId _id) {
//		this._id = _id;
//	}
	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}
	public String getSpeciesName() {
		return speciesName;
	}
	public void setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
	}
	public int getSpeciesCount() {
		return speciesCount;
	}
	public void setSpeciesCount(int speciesCount) {
		this.speciesCount = speciesCount;
	}
	  
	  
}
